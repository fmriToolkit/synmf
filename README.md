# synmf

Symmetric (weighted) nonnegative matrix factorization for graph clustering. So far,
implements the factorization of an adjacency matrix A as A = HH' as described by 
Kuang, Ding, and Park (2012), using a more or less direct translation of the authors
Matlab code. Planned improvements include paralellization of the block Hessian
computation, and porting of some of the more loop-heavy steps to C++.

Also implements the weighted symmetric NMF with multiplicative updates described in Ding, He, and Simon (2005).

Da Kuang, Chris Ding, Haesun Park,
Symmetric Nonnegative Matrix Factorization for Graph Clustering,
The 12th SIAM International Conference on Data Mining (SDM '12), pp. 106--117, 2012.

Ding, C., He, X., & Simon, H. D. (2005, April). On the equivalence of nonnegative matrix factorization and spectral clustering. In Proceedings of the 2005 SIAM International Conference on Data Mining (pp. 606-610). Society for Industrial and Applied Mathematics<.

<h3>Installation</h3>
The package can be installed directly from the Gitlab repository using the `devtools` package with

```r
devtools::install_git('https://gitlab.com/fmriToolkit/synmf.git')
```

Alternately, the repository can be cloned to a local machine and installed through the terminal by running

```
R CMD INSTALL synmf
```
in the directory containing the folder.